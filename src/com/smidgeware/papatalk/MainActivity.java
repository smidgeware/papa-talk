package com.smidgeware.papatalk;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;

public class MainActivity extends Activity {
    private AudioPlayer mPlayer = new AudioPlayer();
    private Button mTalkButton;

    private int[] mPhrases = new int[] {
        R.raw.bear_horse_duck,
        R.raw.bicycle_chow_mein,
        R.raw.confused_me,
        R.raw.dont_give_a_flip,
        R.raw.feel_better,
        R.raw.floats_your_boat,
        R.raw.go_home,
        R.raw.gotta_go,
        R.raw.happy_leon,
        R.raw.hi_howre_you,
        R.raw.invest_recliner,
        R.raw.makes_no_nevermind,
        R.raw.poets_day,
        R.raw.rubber_crutch,
        R.raw.sneak_snack_snuck,
        R.raw.stewed_cat,
        R.raw.tastes_musty,
        R.raw.teepee,
        R.raw.thirty_days
    };

    private int mPhraseIndex = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTalkButton = (Button)findViewById(R.id.talk_button);
        mTalkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPlayer.play(getApplicationContext(), mPhrases[mPhraseIndex]);
                mPhraseIndex = (mPhraseIndex + 1) % mPhrases.length;
            }
        });
    }
    
    @Override
    public void onDestroy() {
        super.onDestroy();
        mPlayer.stop();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }
    
}
